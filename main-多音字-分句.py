#!/usr/bin/env python
# coding: utf-8

# In[3]:


import json
import pickle
import math
from pypinyin import pinyin, Style
import pypinyin
import re
import time



# In[9]:


txt = "【#奔驰女车主不接受4S店道歉# 双方现场交涉言语激烈】4月13日，西安维权奔驰女车主和4S店方见面，双方并未当场和解。4S店相关负责人称，由于出差等原因未及时联系车主，车主则反驳称，可通过电话联系，“没人把你的联系方式给我。”交涉过程中双方言语交锋激烈。#西安奔驰女车主维权#  "
pattern = r',|\.|/|;|\'|`|\[|\]|<|>|\?|:|"|\{|\}|\~|!|@|#|\$|%|\^|&|\(|\)|-|=|\_|\+|，|。|、|；|‘|’|【|】|·|！| |…|（|）'
result_list = re.split(pattern, txt)
print(result_list)


# In[ ]:


# a表示单字出现次数，t表示总和

def read_file(fpath):
    d = {'a':{'t':0}}
    with open(fpath, 'r', encoding="utf-8") as f:
        start  = time.clock()
    # 读取所有行 每行会是一个字符串
        count = 0
        for i in f:
            count += 1
            #每读1w行，print进度
            if count % 10000 == 0:
                print(count)
                print('用时:', time.clock()-start, 's')
                with open('model_duoyin_fenju.dat', 'wb') as f:
                    pickle.dump(d,f)

             #二进制文件读取   
            contents = json.loads(i)['content']
            for content in re.split(pattern, contents):
                content=''.join([ci for ci in content if ci >= '\u4e00' and ci <= '\u9fa5'])

                #break
                #print(content)
                pinyinlist=pinyin(content,style=Style.NORMAL)
                #print(pinyinlist)
                n = len(content)
                #break
                pred = ''
                for k in range(n):
                    c = content[k]
                    if c >= '\u4e00' and c <= '\u9fa5':
                        pred = c+pinyinlist[k][0]
                        break
                if not pred:
                    continue  # 洗洗睡吧
                if not pred in d['a']:
                    d['a'][pred] = 1
                else:
                    d['a'][pred] += 1
                d['a']['t'] += 1
                for k in range(1, n):
                    c = content[k]
                    if c >= '\u4e00' and c <= '\u9fa5':
                        c+=pinyinlist[k][0]
                        # 单字
                        if not c in d['a']:
                            d['a'][c] = 1
                        else:
                            d['a'][c] += 1
                        d['a']['t'] += 1
                        # 双字
                        if not pred in d:
                            d[pred] = {'t':0}
                        if not c in d[pred]:
                            d[pred][c] = 1
                        else:
                            d[pred][c] += 1
                        d[pred]['t'] += 1
                        pred = c
    return d

data_path = 'news2016zh_train.json'

d = read_file(data_path)


# In[ ]:




